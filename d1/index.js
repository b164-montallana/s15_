console.log('Hello World');

// Assignment Operator
let assignmentNumber = 8;

// Aritmitic Operators
//  +, -, *, /, %


// Addition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber);
// shorthand for Addition assignment operator,
assignmentNumber += 2;
console.log(assignmentNumber);

// Same with subtraction/multiplication/division assignment operator (-=, *=, /=)
assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);


// Multiple Operators and Parenthesis
/*
-When multiple operators are applied in a single statement, it follows the PEMDAS(Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule.

-The operatins were done in the ff order:
    1. 3 * 4 = 12
    1. 12 / 5 = 2.4
    1. 1 + 2 = 3
    1. 3 - 2 = 1
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);


// Increment and Decrement Operator
// Operators that add or subtract values by 1 and assigns the value of the variable where the increment/decrement was applied to.

let z = 1;

// Increment
// Pre-fix incrementation.
// The value of "z" is added by 1 before retorning the value and storing it in the variable.
++z;
console.log(z); // 2 - the value of z was added by 1 and is immediately retured.


// Post-fix incrementation
// The value of "z" is retured and stored in the variable "increment" then the value of "z" is increased by one
z++;
console.log(z);// 3 - the value of z was added with one
console.log(z++);// value =3
console.log(z);

console.log(++z);// the new value is returned immediately.

// Pre-fix decrementation
console.log(--z); //same witn pre-fix incrementation behavior but decreases the value.

// Post-fix decrementation
console.log(z--);// same witn post-fix incrementation behavior but decreases the value.


// Type Coercion
    // Is the automatic or implicit conversion of values from one data type to another.
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);



// Adding/concatenating a string and a number will result to a string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// the boolean true is associate with the value of 1.
let numE = true + 1;
console.log(numE);
console.log(typeof numE);

// the boolean true is associate with the value of 0.
let numF = false + 1;
console.log(numF);


// Comparison Operators
let juan = 'juan';

// (==) This is the Equality Operator. (=) This is the assignment Operator.
// The Equality operator checks the operant are equal/have the same content.
// Attempts tp CONVERT AND COMPARE opperands of different data type.

console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'JUAN'); // case sensitive
console.log('juan' == juan); //true

// (!=) This is Inequality Operator
// Checks whether the operant are not equal/have different content

console.log(1 != 1); //false
console.log(1 != 2); //true


// (===) Strictly equality Operator
console.log('Strictly Equality Operator');
console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //true
console.log(0 === false); //true
console.log('juan' === 'JUAN'); // case sensitive
console.log('juan' === juan); //true

// (!==) Strict inequality operator
console.log('Strictly Inequality Operator');
console.log(1 !== 1); //true
console.log(1 !== 2); //false
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'JUAN'); // case sensitive
console.log('juan' !== juan); //true

// Relational Comparison Operators
// Check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = '5500';

// Greater than
console.log(x>y); //false
console.log(w>y); //true

// Less Than
console.log(w<x); //fase
console.log(y<y); //false
console.log(x<1000); //true
console.log(numString < 1000); //false
console.log(numString < 6000); //true - force coercion changed string into number
console.log( numString < 'Jose');//true

// Greater than or equal to
console.log('greater than or equal to');
console.log(w >= 8000);//true

// Less than or Equal to
console.log(x <= y);
console.log(y <= y);

//logical operators
let isAdmin = false;
let isRegister = true;
let isLegalAge = true;

console.log('Logical AND operator');
// Logical AND Operator (&& - Double ampersand)
// Returns true if ALL operands are true

let authorization = isAdmin && isRegister;
console.log(authorization);//false

let authorization1 = isLegalAge && isRegister;
console.log(authorization1);//false


let requiredLevel = 95;
let requiredAge = 18;

let authorization2 = isRegister && requiredLevel === 25;
console.log(authorization2);//false

let authorization3 = isRegister && isLegalAge && requiredLevel === 95;
console.log(authorization3);//true


let userName = 'gamer2022';
let userName2 = 'shadow1991';
let userAge = 15;
let userAge2 = 30;


let registration1 = userName.length > 8 && userAge >= requiredAge;
// .lenght is a property of strings which determine the number of character in the string.
console.log(registration1);//false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);// true


console.log('Logical OR Operator');
// OR operator (|| - Double Pipe)
// returns true if atleast ONE of the operands are true

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement1 = isRegister || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement1);//true

let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2);//false

console.log('NOT operator');
// NOT operator (!)
// It turns a nboolean into the opposite value.

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);

console.log(!isRegister); //false
console.log(!isLegalAge);

let opposite = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite);
console.log(opposite2);


// if, else if, and els statement

// IF statement
// if statement will run a code block if the condition specified is tru or results to true.

// if(true){
//     alert('we just run an if condition');
// }

let numG = -1;

if(numG < 0 ) {
    console.log('Hello')
}

let userName3 = "crusader_1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10) {
    console.log('Welcome to Game Online');
}

if(userLevel3 >= requiredLevel) {
    console.log('You are qualified to join the game');
}

if(userName3.length >= 10 && isRegister && isAdmin) {
    console.log('Thank you for joining the Admin');
} else {
    console.log('You are not ready to be an Admin');
}

// ELSE statement
//  the else statement exucutes a block of codes if all other conditions are false.

if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge) {
    console.log('Thank you for joining the Noobies Guild');
} else {
    console.log('You are too strong to be a noob');
}


// ELSE IF statement

if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredLevel) {
    console.log('Thank you for joining the noobies Guild');
} else if (userLevel < 25) {
    console.log('You are too strong to be a noob');
} else if (userAge3 < requiredAge) {
    console.log('You are too young to join the guild');
} else {
    console.log('Better luck next time.');
}


// if, else if, and else statement with functions

function addNum(num1,num2) {
    //check if the numbers being passed are number types.
    //typeof keyword returns a string which tells the type of data that follows it
    if(typeof num1 === "number" && typeof num2 === "number") {
        console.log('Run only if both arguments passed are number types');
        console.log(num1 + num2);
    } else {
        console.log('One or both of the arguments are not numbers');
    }
}

addNum(5,'2');


// create log in function
 function login(username, password) {
     if(typeof username === 'string' && typeof password === 'string'){
         console.log('Both Arguments are string');
       
         if(username.length >= 8 && password.length >=8 ) {
            console.log('Thank you for logging in');
        } else
            {console.log('Credentials too short.');}
       
       
    } else {
         console.log('One of the arguments is not string');
     }
 }

 login('jane12345', '12121');


//  function with return keyword

let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
    if(windSpeed < 30) {
        return 'Not a typhoon yet.';
    }
    else if(windSpeed <= 61) {
        return 'Tropical depression detected.';
    }
    else if(windSpeed >= 62 && windSpeed <= 88) {
        return 'Tropical Storm detected.';
    }
    else if(windSpeed >= 89 && windSpeed <= 117) {
        return 'Severe Tropical Storm detected.';
    }
    else {
        return 'Typhoon detected.'
    }
}

message = determineTyphoonIntensity(68);
console.log(message);

if (message == 'Tropical Storm detected.'){
    console.warn(message);
}


// Truthy and Falsy
    //Truthy
if(true) {
    console.log('Truthy');
}

if(1) {
    console.log('true');
}

if([]) {
    console.log('Truthy');
}

// Falsy
// -0, "", NaN
if(false){
    console.log('Falsy');
}

if(0) {
    console.log('False');
}

// Ternary Operator
    //Single statement execution

/*
Syntax;
    (expression/condition) ? ifTrue : ifFlase;
Three operand of ternary operator:
1. condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is false
*/

let ternaryResult = (1<18) ? true : false;
console.log(`Result of ternary operator ${ternaryResult}`);

if(1<18) {
    console.log(true);
} else {
    console.log(false);
}

let price = 5000;

price > 1000 ? console.log('price is over 1000') : console.log('price is less than 1000');

let villain = "Harvey Dent";

villain === "Two Face"
? console.log('You lived long enough to be villian')
: console.log("Not quite villainous yet.");

// Ternary operators have an implicit "retirn" staement that without return keyword, 


// else if ternary operator

let a = 7;

a === 5
? console.log(A)
: (a === 10 ? console.log('A is 10') : console.log('A is not 5 or 10'));

// Multiple statement execution

// let name;

// function isOfLegalAge() {
//     name = 'Jhon';
//     return 'You are of the legal age limit';
// }

// function isUnderlAge() {
//     name = 'Jane';
//     return 'You are Under age limit';
// }

// // let age = parseInt(prompt('What is your age?'));
// // console.log(age);

// (?) == else
// () == specifies the condition
// (:) == if
// let legalAge = (age > 18) ? isOfLegalAge() : isUnderlAge();
// console.log(`Result of Ternary operator in functions: ${legalAge}, ${name}`);

// MINI ACTIVITY

// function colorOfTheDay(day) {
//     if(typeof day === 'string'){

//         if(day.toLowerCase() === 'monday'){
//                 alert(`TOday is ${day}, Wear Black`)
//         } else if(day.toLowerCase() === 'tuesday'){
//                 alert(`TOday is ${day}, Wear Green`)
//         } else if(day.toLowerCase() === 'wednesday'){
//                 alert(`TOday is ${day}, Wear Yellow`)
//         } else if(day.toLowerCase() === 'thursday'){
//                 alert(`TOday is ${day}, Wear Red`)
//         } else if(day.toLowerCase() === 'friday'){
//                 alert(`TOday is ${day}, Wear Violet`)
//         } else {
//             alert('Invalid input')
//         }

//     }   else {
//             alert('Invalid Input. Enter a valid day of the week')
//     }
// }

// let day = prompt('Enter a Day');`

// colorOfTheDay(day);


// Switch Statement
/*
Syntax:
    Switch (expression/condition) {
        case value:
            statement;
            break;
        default:
            statement;
            break;
    }
*/ 

// let hero = prompt ('Type a Hero').toLowerCase();

// switch (hero) {
//     case "jose rizal":
//         console.log('National Hero of the Philippines');
//         break;
//     case "george washington":
//         console.log("Hero of the American Revolotion");
//         break;
//     case "hercules":
//         console.log("Legendary Hero Of the Greek");
//         break;
// }


function roleChecker(role) {
    switch(role) {
        case "admin":
            console.log("Welcome Admin, to the dashboard.");
            break;
        case "user":
            console.log("You are not aurhorized to view this page.");
            break;
        case "guest":
            console.log("Go to the registration page ");
            break;
        default:
            console.log("Invalid Role");
    }
}

roleChecker("admin");



// try-catch-finally Statement
// this is used for error handling

function showIntensityAlert(windSpeed) {
    // Attempt to execute a code
    try {
        alert(determineTyphoonIntensity(windSpeed));
    } catch (error) {
        // error/err are commonly used variable for storing error
        console.log(typeof error);
        console.warn(error.message)
    } finally {
        // Continue execution of code REGARDLESS of success or failure of code execution in the 'try block to handle/resolve errors.
        // optional
        alert('intensity updates will show new alert')
    }
}

showIntensityAlert(68);

// throw - user-defined exception
// Execution of the current function will stop

const number = 40;

try {
    if (number > 50) {
        console.log('Success');
    } else {
        // user-defined throw staement
        throw Error('The number is low');

    }
    // if throw executes the below code will not execute
    console.log('Hello world');
}
catch (error) {
    console.log('An error caught');
    console.warn(error.message)
}
finally {
    console.log("Please add a higher number");
}

// another example

function getArea(width, height) {
    if(isNaN(width) || isNaN(height)) {
        throw 'Parameter is not a Number'
    }
}
try {
    getArea(3, 'A');
}
catch(e) {
    console.error(e);
}
finally {
    alert("Number only")
}